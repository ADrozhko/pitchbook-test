package com.pitchbook.spd.controller;

import com.pitchbook.spd.service.ISearchService;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/")
public class SearchController {

    @Autowired
    ISearchService searchService;

    @RequestMapping(value = "" ,method = RequestMethod.GET)
    public String getSearchPage() {

        return "search/search";
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getSearchPageWithParameters(ModelMap map,
                                              @RequestParam(value = "q", required = false) String q,
                                              @RequestParam(value = "p", required = false, defaultValue = "1") Integer p) {
        map.addAttribute("query", q);
        map.addAttribute("pageNumber", p);
        return "search/search";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> search(String q, Integer p) throws IOException, ParseException {
        Map<String, Object> searchResult = new HashMap<>();
        searchResult.put("books", searchService.search(q, p));
        searchResult.put("resultCount", searchService.getTotalHits());
        searchResult.put("searchWord", q);
        searchResult.put("queryTime", searchService.getTimeOfQuery());
        return searchResult;
    }
}
