package com.pitchbook.spd.util;



import java.util.regex.Pattern;

public class Const {

    public static final Integer BEGIN_INDEXING_DEPTH = 0;

    public static final Integer SEARCH_RESULT_PER_PAGE = 10;
    public static final String INDEX_DIR = "/index";

}
