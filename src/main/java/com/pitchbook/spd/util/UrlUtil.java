//package com.pitchbook.spd.util;
//
//
//import org.apache.log4j.Logger;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.MalformedURLException;
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.regex.Matcher;
//
//public class UrlUtil {
//
//    private static final Logger logger = Logger.getLogger(UrlUtil.class);
//    /*
//        URL VALIDATION
//     */
//
//    public static String validateSubUrl(String url, String subUrl) {
//        url = clearUrl(url);
//        subUrl = clearUrl(subUrl);
//        subUrl = resolveIfNotAbsolute(url, subUrl);
//        if(url.equals(subUrl)) {
//            return null;
//        }
//        if(subUrl == null) {
//            return null;
//        }
//        if(!isValidURL(url)) {
//            return null;
//        }
//        return subUrl;
//    }
//
//    public static String resolveIfNotAbsolute(String url, String subUrl) {
//        if (null == subUrl) {
//            return url;
//        }
//        url = clearUrl(url);
//        subUrl = clearUrl(subUrl);
//        if(isAbsoluteURL(subUrl)) {
//            return subUrl;
//        } else {
//            try {
//                if(subUrl.matches("(/[\\w~,;\\-\\./?%&+=]*)")) {
//                    return new URI(url).resolve(subUrl).toString();
//                }
//            } catch (URISyntaxException e) {
//                logger.error("Error on parsing url: '" + subUrl + "' ; " + e.getLocalizedMessage());
//                return null;
//            }
//        }
//        return null;
//    }
//
//    private static String clearUrl(String link) {
//        link = link.replaceAll("'","");
//        return link.replaceAll("\"", "").trim();
//    }
//
//    public static boolean isValidURL(String url) {
//        URL u = null;
//        try {
//            u = new URL(url);
//            u.openConnection();
//        } catch (MalformedURLException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            return false;
//        } catch (IOException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            return false;
//        }
//
//        try {
//            URI uri = u.toURI();
//            if ("mailto".equals(uri.getScheme())) {
//                return false;
//            }
//            if (uri.getHost() == null) {
//                return false;
//            }
//        } catch (URISyntaxException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            return false;
//        }
//        return true;
//    }
//
//    private static boolean isAbsoluteURL(String url) {
//        try {
//            return new URI(url).isAbsolute();
//        } catch (URISyntaxException e) {
//            logger.error("Skipp not absolute url: '" + url + "' ; " + e.getLocalizedMessage());
//            return false;
//        }
//    }
//
//    /*
//        WORK WITH URL
//     */
//
//    public static List<String> getLinksFromPage(String pageText)  {
//        Matcher pageMatcher = Const.LINK_TAG_PATTERN.matcher(pageText);
//        List<String> links = new ArrayList<>();
//        while(pageMatcher.find()){
//            String linkTag = pageMatcher.group();
//            Matcher linkMatcher = Const.LINK_PATTERN.matcher(linkTag);
//            if(linkMatcher.find()) {
//                String link = linkMatcher.group(1);
//                    links.add(link);
//            }
//        }
//        return links;
//    }
//
//    public static URLConnection getUrlConnection(String url) {
//        try {
//            return new URL(url).openConnection();
//        } catch (IOException e) {
//            logger.error("Skipp. Can't get connection to url: '" + url + "' ; " + e.getLocalizedMessage());
//            return null;
//        }
//    }
//
//    public static String getTextFromUrl(String url) {
//        BufferedReader in = null;
//        try {
//            in = new BufferedReader(new InputStreamReader(
//                    getUrlConnection(url).getInputStream()));
//        } catch (IOException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            return null;
//        } catch (NullPointerException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            return null;
//        }
//
//        String inputLine;
//        StringBuilder pageText = new StringBuilder();
//        try {
//            while ((inputLine = in.readLine()) != null)
//                pageText.append(inputLine);
//        } catch (IOException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            try {
//                in.close();
//            } catch (IOException e1) {
//                logger.error("Skipp Invalid url: '" + url + "' ; " + e1.getLocalizedMessage());
//                e1.printStackTrace();
//            }
//        }
//        try {
//            in.close();
//        } catch (IOException e) {
//            logger.error("Skipp Invalid url: '" + url + "' ; " + e.getLocalizedMessage());
//            e.printStackTrace();
//        }
//        return pageText.toString();
//    }
//
//    public static String getPageTitle(String pageText) {
//        if (null == pageText)  {
//            return "No Title";
//        }
//        Matcher titleMatcher = Const.TITLE_PATTERN.matcher(pageText);
//        if(titleMatcher.find()) {
//            return titleMatcher.group(1);
//        }
//        return "No Title";
//    }
//
//    public static String removeHtmlTags(String pageText) {
//        String text = pageText;
//        if(pageText.contains("<body")) {
//            text =  pageText.substring( pageText.indexOf("<body"),  pageText.lastIndexOf("body>"));
//        }
//        text = text.replaceAll("<script.*?</script>", "");
//        text = text.replaceAll("<noscript.*?</noscript>", "");
//        //text = text.replaceAll("<a.*?</a>", "");
//        text = text.replaceAll("\\s+", " ");
//        text = text.replaceAll("<.*?>","");
//        text = removeHtmlSpecialCharacter(text);
//
//
//        return text.replaceAll("\\s+", " ");
//
//    }
//
//    private static String removeHtmlSpecialCharacter(String text) {
//        return text.replaceAll("&quot;","\"")
//                    .replaceAll("&amp;", "&")
//                    .replaceAll("&rsquo", ",")
//                    .replaceAll("&copy;", "");
//    }
//}
//
