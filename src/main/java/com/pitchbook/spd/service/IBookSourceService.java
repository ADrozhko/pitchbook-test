package com.pitchbook.spd.service;


import com.pitchbook.spd.entity.BookSource;

import java.util.List;

public interface IBookSourceService {

    public List<BookSource> getSourcesByBookId(Integer bookId);
}
