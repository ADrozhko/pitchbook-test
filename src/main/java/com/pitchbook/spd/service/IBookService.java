package com.pitchbook.spd.service;


import com.pitchbook.spd.entity.Book;

import java.util.List;

public interface IBookService {
    public List<Book> getAllBooks();

    public Book getBookById(Integer bookId);

    public List<Book> getBooksBuIds(List<Integer> ids);
}
