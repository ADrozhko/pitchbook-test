package com.pitchbook.spd.service;

import com.pitchbook.spd.entity.Genre;

import java.util.List;

public interface IGenreService {

    public List<Genre> getGenresByBookId(Integer bookId);
}
