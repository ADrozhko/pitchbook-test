package com.pitchbook.spd.service;

import com.pitchbook.spd.entity.Type;

public interface ITypeService {
    public Type getTypeByBookSourceId(Integer bookSourceId);
}
