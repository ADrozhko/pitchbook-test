package com.pitchbook.spd.service;


import com.pitchbook.spd.bean.PageBean;
import com.pitchbook.spd.entity.Book;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.List;

public interface ISearchService{


    public List<Book> search(String searchWord, Integer pageNumber) throws IOException, ParseException;

    public Integer getTotalHits();

    public Double getTimeOfQuery();
}
