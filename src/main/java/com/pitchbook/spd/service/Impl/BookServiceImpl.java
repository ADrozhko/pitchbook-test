package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.IBookDao;
import com.pitchbook.spd.entity.Book;
import com.pitchbook.spd.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements IBookService {

    @Autowired
    private IBookDao bookDao;

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    @Override
    public Book getBookById(Integer bookId) {
        return bookDao.getBookById(bookId);
    }

    @Override
    public List<Book> getBooksBuIds(List<Integer> ids) {
        return bookDao.getBooksBuIds(ids);
    }
}
