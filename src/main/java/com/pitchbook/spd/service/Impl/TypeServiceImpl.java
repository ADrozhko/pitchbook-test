package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.ITypeDao;
import com.pitchbook.spd.entity.Type;
import com.pitchbook.spd.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeServiceImpl implements ITypeService{

    @Autowired
    private ITypeDao typeDao;

    @Override
    public Type getTypeByBookSourceId(Integer bookSourceId) {
        return typeDao.getTypeByBookSourceId(bookSourceId);
    }
}
