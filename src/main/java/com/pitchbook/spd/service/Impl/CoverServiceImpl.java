package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.ICoverDao;
import com.pitchbook.spd.entity.Cover;
import com.pitchbook.spd.service.ICoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoverServiceImpl implements ICoverService{

    @Autowired
    private ICoverDao coverDao;

    @Override
    public List<Cover> getCoversByBookId(Integer bookId) {
        return coverDao.getCoversByBookId(bookId);
    }
}
