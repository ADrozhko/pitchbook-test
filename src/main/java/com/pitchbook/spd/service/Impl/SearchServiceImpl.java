package com.pitchbook.spd.service.Impl;


import com.pitchbook.spd.entity.Book;
import com.pitchbook.spd.service.IBookService;
import com.pitchbook.spd.service.ISearchService;
import com.pitchbook.spd.util.Const;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.sandbox.queries.DuplicateFilter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements ISearchService {


    @Autowired
    private Directory directory;

    @Autowired
    private Analyzer analyzer;
    @Autowired
    private IBookService bookService;

    private Logger logger = Logger.getLogger(this.getClass());

    private ThreadLocal<Integer> totalHints = new ThreadLocal<>();

    private ThreadLocal<Double> timeOfQuery = new ThreadLocal<>();

    @Override
    public Integer getTotalHits() {
        return totalHints.get();
    }

    @Override
    public Double getTimeOfQuery() {
        return timeOfQuery.get();
    }

    @Async
    @Override
    public List<Book> search(String searchWord, Integer pageNumber) {
        long startTime = System.nanoTime();
        List<Book> books = new ArrayList<>();
        try(IndexReader reader = DirectoryReader.open(directory)) {
            IndexSearcher searcher = new IndexSearcher(reader);
            DuplicateFilter df = new DuplicateFilter("title");
            df.setKeepMode(DuplicateFilter.KeepMode.KM_USE_FIRST_OCCURRENCE);
            Integer countPages = pageNumber * Const.SEARCH_RESULT_PER_PAGE;
            TopScoreDocCollector collector = TopScoreDocCollector.create(countPages, true);
            searcher.search(query(searchWord), df, collector);
            ScoreDoc[] hits = collector.topDocs().scoreDocs;

            totalHints.set(collector.getTotalHits());
            logger.info("On request: '" + searchWord + "' was found: " + hits.length + " hits.");
            List<Integer> bookIds = new ArrayList<>();
            for (int i = countPages - Const.SEARCH_RESULT_PER_PAGE; i < countPages; i++) {
                if(i >= hits.length) {
                    break;
                }
                Document d = null;
                try {
                    d = searcher.doc(hits[i].doc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bookIds.add(Integer.valueOf(d.get("bookId")));
//                Book book = new Book();
//                book.setId(Integer.valueOf(d.get("bookId")));
//                book.setDescription(d.get("description"));
//                book.setTitle(d.get("title"));
//                books.add(book);
//                books.add(bookService.getBookById(Integer.valueOf(d.get("bookId"))));
            }
            long endTime = System.nanoTime();
            this.timeOfQuery.set(Math.round( (endTime - startTime)/1000000000.0 * 100.0 ) / 100.0);
            return  bookService.getBooksBuIds(bookIds);
        } catch (IOException ignored) {
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();

    }


    private Query query(String searchWord) throws ParseException {
        return new MultiFieldQueryParser(Version.LUCENE_40, new String[]{"bookId", "title", "text"}, analyzer).parse(searchWord);
    }


}
