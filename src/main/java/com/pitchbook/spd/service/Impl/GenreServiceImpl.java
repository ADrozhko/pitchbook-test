package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.IGenreDao;
import com.pitchbook.spd.entity.Genre;
import com.pitchbook.spd.service.IGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements IGenreService{

    @Autowired
    private IGenreDao genreDao;

    @Override
    public List<Genre> getGenresByBookId(Integer bookId) {
        return genreDao.getGenresByBookId(bookId);
    }
}
