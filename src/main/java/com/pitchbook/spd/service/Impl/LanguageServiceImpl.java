package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.ILanguageDao;
import com.pitchbook.spd.entity.Language;
import com.pitchbook.spd.service.ILanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LanguageServiceImpl implements ILanguageService{

    @Autowired
    private ILanguageDao languageDao;

    @Override
    public Language getLanguageByBookId(Integer bookId) {
        return languageDao.getLanguageByBookId(bookId);
    }
}
