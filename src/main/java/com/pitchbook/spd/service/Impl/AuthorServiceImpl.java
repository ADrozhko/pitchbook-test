package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.IAuthorDao;
import com.pitchbook.spd.entity.Author;
import com.pitchbook.spd.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements IAuthorService{

    @Autowired
    private IAuthorDao authorDao;

    @Override
    public List<Author> getAuthorsByBookId(Integer bookId) {
        return authorDao.getAuthorsByBookId(bookId);
    }
}
