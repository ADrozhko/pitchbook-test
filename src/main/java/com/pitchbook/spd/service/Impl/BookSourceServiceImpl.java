package com.pitchbook.spd.service.Impl;

import com.pitchbook.spd.dao.IBookSourceDao;
import com.pitchbook.spd.entity.BookSource;
import com.pitchbook.spd.service.IBookSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookSourceServiceImpl implements IBookSourceService{

    @Autowired
    private IBookSourceDao bookSourceDao;

    @Override
    public List<BookSource> getSourcesByBookId(Integer bookId) {
        return bookSourceDao.getSourcesByBookId(bookId);
    }
}
