package com.pitchbook.spd.service;


import com.pitchbook.spd.entity.Cover;

import java.util.List;

public interface ICoverService {
    public List<Cover> getCoversByBookId(Integer bookId);
}
