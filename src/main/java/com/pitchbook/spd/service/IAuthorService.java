package com.pitchbook.spd.service;


import com.pitchbook.spd.entity.Author;

import java.util.List;

public interface IAuthorService {

    public List<Author> getAuthorsByBookId(Integer bookId);
}
