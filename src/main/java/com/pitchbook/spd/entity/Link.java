package com.pitchbook.spd.entity;

public class Link {

    private Integer id;
    private String url;
    private Boolean checked = false;
    private Boolean error = false;
    private OPDS opds;

    public Integer getId() {
        return id;
    }

    public Link(String url) {
        this.url = url;
    }
    public Link(OPDS opds) {
        this.opds = opds;
    }
    public Link(OPDS opds, String url) {
        this.opds = opds;
        this.url = url;
    }

    public Link() {

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public OPDS getOpds() {
        return opds;
    }

    public void setOpds(OPDS opds) {
        this.opds = opds;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
}
