package com.pitchbook.spd.entity;

public class BookSource {

    private Integer id;
    private Book book;
    private OPDS opds;
    private String url;
    private Type type;

    public BookSource() {}

    public BookSource(String url, Type type, OPDS opds) {
        this.url = url;
        this.type = type;
        this.opds = opds;
    }
    public BookSource(Integer id, String url, Type type) {
        this.id = id;
        this.url = url;
        this.type = type;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public OPDS getOpds() {
        return opds;
    }

    public void setOpds(OPDS opds) {
        this.opds = opds;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
