package com.pitchbook.spd.entity;

public class Cover {

    private Integer id;
    private String url;

    public Cover() {

    }

    public Cover(String url) {
        this.url = url;
    }

    public Cover(Integer id, String url) {
        this.id = id;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

