package com.pitchbook.spd.entity;


import java.util.Date;
import java.util.List;

public class Book {

    private Integer id;
    private String title;
    private Date publishDate;
    private String description;
    private List<Author> authors;
    private List<Genre> genres;
    private Language language;
    private List<Cover> covers;
    private List<BookSource> sources;

    public Book() {}
    public Book(String title, String description, Date publishDate, List<Author> authors, List<Genre> genres, Language language, List<BookSource> bookSources, List<Cover> covers) {
        this.title = title;
        this.description = description;
        this.publishDate = publishDate;
        this.authors = authors;
        this.genres = genres;
        this.language = language;
        this.sources = bookSources;
        this.covers = covers;
        bookSources.forEach(bs -> bs.setBook(this));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<BookSource> getSources() {
        return sources;
    }

    public void setSources(List<BookSource> sources) {
        this.sources = sources;
    }

    public List<Cover> getCovers() {
        return covers;
    }

    public void setCovers(List<Cover> covers) {
        this.covers = covers;
    }
}
