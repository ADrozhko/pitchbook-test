package com.pitchbook.spd.bean;


public class PageBean {

    private Integer id;
    private String title;
    private String url;
    private String text;

    public PageBean() {

    }

    public PageBean(Integer id, String title, String url, String text) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
