package com.pitchbook.spd.dao;

import com.pitchbook.spd.entity.Type;

public interface ITypeDao {

    public Type getTypeByBookSourceId(Integer bookSourceId);

}
