package com.pitchbook.spd.dao;

import com.pitchbook.spd.entity.Author;

import java.util.List;

public interface IAuthorDao {

    public List<Author> getAuthorsByBookId(Integer bookId);
}
