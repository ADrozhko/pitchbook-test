package com.pitchbook.spd.dao;

import com.pitchbook.spd.entity.BookSource;

import java.util.List;

public interface IBookSourceDao {

    public List<BookSource> getSourcesByBookId(Integer bookId);

}
