package com.pitchbook.spd.dao;

import com.pitchbook.spd.entity.Book;

import java.util.List;

public interface IBookDao {

    public List<Book> getAllBooks();

    public List<Book> getAllNotIndexedBooks();

    public Book getBookById(Integer id);

    public void setBookIndexed(Integer id);

    public List<Book> getBooksBuIds(List<Integer> ids);

}
