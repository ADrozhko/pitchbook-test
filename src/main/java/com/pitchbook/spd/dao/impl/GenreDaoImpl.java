package com.pitchbook.spd.dao.impl;

import com.pitchbook.spd.dao.IGenreDao;
import com.pitchbook.spd.entity.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class GenreDaoImpl implements IGenreDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Genre> getGenresByBookId(Integer bookId) {
        return null;
    }
}
