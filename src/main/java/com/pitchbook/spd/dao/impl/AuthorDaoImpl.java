package com.pitchbook.spd.dao.impl;


import com.pitchbook.spd.dao.IAuthorDao;
import com.pitchbook.spd.entity.Author;
import com.pitchbook.spd.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AuthorDaoImpl implements IAuthorDao{

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Author> getAuthorsByBookId(Integer bookId) {
        String sql = "SELECT * FROM AUTHOR JOIN AUTHOR_BOOK AS AB WHERE AB.BOOK_ID = ? AND ID = AB.AUTHOR_ID";
        List<Author> authors = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, bookId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                authors.add(new Author(rs.getInt("ID"), rs.getString("NAME")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }
}
