package com.pitchbook.spd.dao.impl;

import com.pitchbook.spd.dao.IAuthorDao;
import com.pitchbook.spd.dao.IBookDao;
import com.pitchbook.spd.dao.IBookSourceDao;
import com.pitchbook.spd.entity.Book;
import com.pitchbook.spd.service.IAuthorService;
import com.pitchbook.spd.service.IBookSourceService;
import com.pitchbook.spd.service.ICoverService;
import jdk.nashorn.internal.runtime.arrays.ArrayIndex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BookDaoImpl implements IBookDao {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private IBookSourceService bookSourceService;

    @Autowired
    private ICoverService coverService;

    @Override
    public List<Book> getAllBooks() {
        String sql = "SELECT * FROM BOOK";
        List<Book> books = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            ResultSet rs = conn.prepareStatement(sql).executeQuery();
            while(rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("ID"));
                book.setTitle(rs.getString("TITLE"));
                book.setDescription(rs.getString("DESCRIPTION"));
                book.setAuthors(authorService.getAuthorsByBookId(book.getId()));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public List<Book> getAllNotIndexedBooks() {
        String sql = "SELECT * FROM BOOK WHERE INDEXED=0";
        List<Book> books = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            ResultSet rs = conn.prepareStatement(sql).executeQuery();
            while(rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("ID"));
                book.setTitle(rs.getString("TITLE"));
                book.setDescription(rs.getString("DESCRIPTION"));
                book.setAuthors(authorService.getAuthorsByBookId(book.getId()));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public Book getBookById(Integer id) {
        String sql = "SELECT * FROM BOOK WHERE ID = ?";
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("ID"));
                book.setTitle(rs.getString("TITLE"));
                book.setDescription(rs.getString("DESCRIPTION"));
                book.setAuthors(authorService.getAuthorsByBookId(book.getId()));
                book.setSources(bookSourceService.getSourcesByBookId(book.getId()));
                book.setCovers(coverService.getCoversByBookId(book.getId()));
                return book;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setBookIndexed(Integer id) {
        String sql = "UPDATE BOOK SET INDEXED=? WHERE ID = ?";
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setBoolean(1, true);
            ps.setInt(2, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Book> getBooksBuIds(List<Integer> ids) {
        String sql = "SELECT * FROM BOOK WHERE ID IN(" +  String.join(",", ids.stream().map(String::valueOf).collect(Collectors.toList())) + ")";
        List<Book> books = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            Integer[] array = ids.stream().toArray(Integer[]::new);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("ID"));
                book.setTitle(rs.getString("TITLE"));
                book.setDescription(rs.getString("DESCRIPTION"));
                book.setAuthors(authorService.getAuthorsByBookId(book.getId()));
                book.setSources(bookSourceService.getSourcesByBookId(book.getId()));
                book.setCovers(coverService.getCoversByBookId(book.getId()));
                books.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }


}
