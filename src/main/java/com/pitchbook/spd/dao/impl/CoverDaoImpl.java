package com.pitchbook.spd.dao.impl;

import com.pitchbook.spd.dao.ICoverDao;
import com.pitchbook.spd.entity.Cover;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CoverDaoImpl implements ICoverDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Cover> getCoversByBookId(Integer bookId) {
        String sql = "SELECT * FROM COVER JOIN BOOK_COVER AS BC WHERE BC.BOOK_ID = ? AND ID = BC.COVER_ID";
        List<Cover> covers = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, bookId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                covers.add(new Cover(rs.getInt("ID"), rs.getString("URL")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return covers;
    }
}
