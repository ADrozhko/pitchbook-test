package com.pitchbook.spd.dao.impl;

import com.pitchbook.spd.dao.ITypeDao;
import com.pitchbook.spd.entity.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class TypeDaoImpl implements ITypeDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public Type getTypeByBookSourceId(Integer bookSourceId) {
        String sql = "SELECT * FROM TYPE JOIN BOOK_SOURCE_TYPE AS BST WHERE BST.BOOK_SOURCE_ID = ? AND ID = BST.TYPE_ID";
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, bookSourceId);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                 return new Type(rs.getInt("ID"), rs.getString("NAME"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
