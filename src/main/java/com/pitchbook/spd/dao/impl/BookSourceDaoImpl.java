package com.pitchbook.spd.dao.impl;

import com.pitchbook.spd.dao.IBookSourceDao;
import com.pitchbook.spd.entity.Book;
import com.pitchbook.spd.entity.BookSource;
import com.pitchbook.spd.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookSourceDaoImpl implements IBookSourceDao {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private ITypeService typeService;

    @Override
    public List<BookSource> getSourcesByBookId(Integer bookId) {
        String sql = "SELECT * FROM BOOK_SOURCE WHERE BOOK_ID = ?";
        List<BookSource> sources = new ArrayList<>();
        try(Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, bookId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                sources.add(new BookSource(rs.getInt("ID"), rs.getString("URL"), typeService.getTypeByBookSourceId(rs.getInt("ID"))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sources;
    }
}
