package com.pitchbook.spd.dao;


import com.pitchbook.spd.entity.Cover;

import java.util.List;

public interface ICoverDao {

    public List<Cover> getCoversByBookId(Integer bookId);

}
