package com.pitchbook.spd.dao;


import com.pitchbook.spd.entity.Language;

public interface ILanguageDao {

    public Language getLanguageByBookId(Integer bookId);

}
