package com.pitchbook.spd.dao;

import com.pitchbook.spd.entity.Genre;

import java.util.List;

public interface IGenreDao {

    public List<Genre> getGenresByBookId(Integer bookId);

}
