package com.pitchbook.spd.manager;


import com.pitchbook.spd.dao.IBookDao;
import com.pitchbook.spd.entity.Author;
import com.pitchbook.spd.entity.Book;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class IndexManager {

    @Autowired
    IBookDao bookDao;

    @Autowired
    Directory directory;

    @Autowired
    IndexWriterConfig indexWriterConfig;

    private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private Logger logger = Logger.getLogger(this.getClass());
    private IndexWriter writer = null;

    @PreDestroy
    public void clean() {
        executorService.shutdown();
    }

    @PostConstruct
    public void init() {
        try {
            this.writer = new IndexWriter(directory, indexWriterConfig);
        } catch (IOException e) {
            e.printStackTrace();
        }
        indexBooks();
    }

    private void indexBooks() {
        executorService.scheduleWithFixedDelay(() -> {
            System.out.println("Indexing Books");
            List<Book> books = bookDao.getAllNotIndexedBooks();
            books.forEach(this::indexBook);
        }, 0, 5, TimeUnit.MINUTES);
    }

    private void indexBook(Book book) {
        Document doc = new Document();
        doc.add(new IntField("bookId", book.getId(), Field.Store.YES));
        doc.add(new TextField("title", book.getTitle().toLowerCase(), Field.Store.YES));
        doc.add(new TextField("description", book.getDescription().toLowerCase(), Field.Store.YES));
        bookDao.setBookIndexed(book.getId());
//        book.getAuthors().forEach(a -> indexAuthor(w, a));
        try {
            writer.addDocument(doc);
            writer.commit();
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("Cant index book : " + book.getTitle());
        }
    }

    private void indexAuthor(Author author) {
        Document doc = new Document();
        doc.add(new IntField("authorId", author.getId(), Field.Store.YES));
        doc.add(new StringField("authorName", author.getName().toLowerCase(), Field.Store.YES));
        try {
            writer.addDocument(doc);
        } catch (IOException e) {
            e.printStackTrace();
            logger.warn("Cant index author : " + author.getName());
        }
    }
}
