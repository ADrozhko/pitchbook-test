$(document).ready(function() {
    var searchPageViewModel = new app.SearchViewModel();
    searchPageViewModel.init($("#query").val(), $("#pageNumber").val())
    ko.applyBindings(searchPageViewModel);

    $("#searchWord").on("keydown keypress keyup onchange‏", function() {
        $(".search-wrapper-main").addClass("search-wrapper-result");
        $(".search-wrapper-main").removeClass("search-wrapper-main");
    });

    String.prototype.insert = function (index, string) {
        if (index > 0)
            return this.substring(0, index) + string + this.substring(index, this.length);
        else
            return string + this;
    };
});

var app = app || {};
ko.bindingHandlers['class'] = {
    'update': function (element, valueAccessor) {
        if (element['__ko__previousClassValue__']) {
            $(element).removeClass(element['__ko__previousClassValue__']);
        }
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).addClass(value);
        element['__ko__previousClassValue__'] = value;
    }
};

app.BookSource = function(bookSource) {
    this.links = ko.observableArray([]);
    this.types = ko.observableArray([]);
    if(null != bookSource) {

    }
};
app.Author = function(author) {
    this.id = ko.observable(null);
    this.name = ko.observable("");
    if(null != author) {

    }
};
app.Book = function (userPlainObject, searchWord) {
    this.id = ko.observable(null);
    this.title = ko.observable("");
    this.description = ko.observable("");
    this.authors = ko.observableArray([]);
    this.covers = ko.observableArray([]);
    this.links = ko.observable("");
    this.language = ko.observable("");
    if (userPlainObject != null) {
        this.id(userPlainObject.id);
        this.title(getSearchedTitleFragment(userPlainObject.title, searchWord));
        this.description(getSearchedTextFragment(userPlainObject.description, searchWord));
        this.authors(userPlainObject.authors);
        this.links(userPlainObject.sources);
        this.covers(userPlainObject.covers);
    }
};

app.PageData = function () {
    this.limit = ko.observable();
    this.resultCount = ko.observable();
    this.books = ko.observableArray([]);
    this.searchWord = ko.observable();
    this.queryTime = ko.observable();
};

app.SearchViewModel = function () {
    var self = this;

    this.pageData = ko.observable(new app.PageData({}));

    this.noData = ko.observable();
    this.resultPerPage = ko.observable(10);
    this.pageNumber = ko.observable(1);

    this.sortResultValue = ko.observable("Relative");

    this.nextPage = function(){
        self.pageNumber(parseInt(self.pageNumber()) + 1);
        self.getSearchData();
    };
    this.prevPage = function() {
        self.pageNumber(parseInt(self.pageNumber()) - 1);
        self.getSearchData();
    };

    this.sortResult = function () {
      if(self.sortResultValue() === "Relative") {
          self.sortResultValue("Alphabet");
          self.pageData().books.sort(function(left, right) {
              return left.id() == right.id() ? 0 : (left.id() < right.id() ? -1 : 1) })
      } else {
          self.sortResultValue("Relative");
          self.pageData().books.sort(function(left, right) {
              var leftTitle = left.title().replace(/\W/g, '').toLowerCase();
              var rightTitle = right.title().replace(/\W/g, '').toLowerCase();
              return leftTitle == rightTitle ? 0 : (leftTitle < rightTitle ? -1 : 1)
          });

      }
    };

    this.obtainedPageData = ko.observable(null);
    this.obtainedPageData.subscribe(function (obtainedPageData) {
        self.pageData(new app.PageData(obtainedPageData));
        self.pageData().books(ko.utils.arrayMap(obtainedPageData.books, function (book) {
            return new app.Book(book, obtainedPageData.searchWord);
        }));
        self.pageData().resultCount(obtainedPageData.resultCount);
        self.pageData().searchWord(obtainedPageData.searchWord);
        self.pageData().queryTime(obtainedPageData.queryTime);
        self.searchWord(obtainedPageData.searchWord);
        self.sortResultValue("Alphabet");
    });

    this.searchWord = ko.observable("");

    this.init = function(query, pageNumber) {
        if(undefined != pageNumber && pageNumber != "") {
            self.pageNumber(pageNumber);
        }
        if(undefined != query && query != "") {
            self.searchWord(query);
            self.getSearchData()
        }


    };

    this.getSearchData = function () {
        var data = {
            q: self.searchWord(),
            p: self.pageNumber()
        };
        if(null != self.searchWord() && self.searchWord() != "") {
            $.post("/search", data, function (pageData) {
                self.obtainedPageData(pageData);
                window.history.pushState('searchPage', 'search', prepareURL(this.url));
            });
        }

    };

    function prepareURL(url) {
        url = url + "?q=" + self.searchWord();
        url = url + "&p=" + self.pageNumber();
        return url;
    }

};

function getSearchedTextFragment (text, searchWord) {
    if(text.toLowerCase().indexOf(searchWord.toLowerCase()) != -1) {
        text = text.toLowerCase().insert(text.toLowerCase().indexOf(searchWord.toLowerCase()), "<span class='background-yellow'>")
        text = text.toLowerCase().insert(text.toLowerCase().indexOf(searchWord.toLowerCase()) + searchWord.length, "</span>")
        return text.substring(text.toLowerCase().indexOf(searchWord.toLowerCase()) - 100, text.toLowerCase().indexOf(searchWord.toLowerCase()) + 100)
    } else {
        return text.substring(text.indexOf(' ') , text.indexOf(' ') + 200)
    }
}

function getSearchedTitleFragment (title, searchWord) {
    if(title.toLowerCase().indexOf(searchWord.toLowerCase()) != -1) {
        title = title.insert(title.toLowerCase().indexOf(searchWord.toLowerCase()), "<span class='background-yellow'>")
        title = title.insert(title.toLowerCase().indexOf(searchWord.toLowerCase()) + searchWord.length, "</span>")
        return title;
    } else {
        return title;
    }
}
