$(document).ready(function(){
    $("#indexUrl").on("keydown keypress keyup", function() {
        $("#error").text("");
    });
   $("#indexingForm").submit(function(event) {
       if($("#indexUrl").val() == "") {
           event.preventDefault();
       }
       if(!isUrlValid($("#indexUrl").val())) {
           event.preventDefault();
           $("#error").text("Invalid url. Try again.")
       } else {
           $("#submitIndex").attr("disable", true)
           $("#loading").removeAttr("hidden");
       }
   })
});

function isUrlValid(url) {
    return new RegExp(/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/).test(url);
}