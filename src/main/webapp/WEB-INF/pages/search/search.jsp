<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="../include/header.jsp" />
<script src="http://knockoutjs.com/downloads/knockout-3.2.0.js"></script>
<script src="/resources/js/search.js"></script>
<body>
    <input type="hidden" id="query" value="${query}">
    <input type="hidden" id="pageNumber" value="${pageNumber}">
    <div class="content">
        <c:choose>
            <c:when test="${empty param.q}">
                <div class="search-wrapper-main">
            </c:when>
            <c:when test="${!empty param.q}">
                <div class="search-wrapper-result">
            </c:when>
        </c:choose>
            <div class="search">
                <span class="search-title">Doodle</span>
                        <input data-bind="value: searchWord" type="text" id="searchWord" name="q" class="search-query-input">
                        <button type="submit" id="submitSearch"  name="" data-bind="click: getSearchData()" class="search-submit-button">Search</button>
            </div>
            <div class="search-filter" data-bind="visible: searchWord() && pageData().books().length > 0">
                <a href="#" data-bind="click: sortResult(),text: 'Sort by ' + sortResultValue()" class="tools">Search tools</a>
            </div>
        </div>
        <div class="result-amount" data-bind="visible: searchWord() && pageData().books().length > 0">
            Estimated number of results: <span data-bind="text: pageData().resultCount()"></span> (<span data-bind="text: pageData().queryTime()"></span> sec)
        </div>
        <%--<div class="count-of-results" ></div>--%>
        <div class="no-search-result" data-bind="visible: pageData().resultCount() == 0 ">
            <p>Request <span data-bind="text: pageData().searchWord()"></span> did not match any documents. </p>
            <p>Advice:</p>
            <ul>
                <li>Make sure all words are spelled correctly.</li>
                <li>Try different keywords.</li>
                <li>Try more general keywords.</li>
            </ul>
        </div>
        <div class="results" >
            <jsp:include page="searchResultSingleLine.jsp"/>
            <div class="" data-bind="">
                <ul class="pagination">
                    <li class="previous" data-bind="click: prevPage, visible: pageNumber() > 1">
                        <a href="#">Previous</a>
                    </li>
                    <li class="next"
                        data-bind="click: nextPage, visible: pageNumber() * resultPerPage() < pageData().resultCount() ">
                        <a href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
<jsp:include page="../include/footer.jsp" />