<div  data-bind="foreach: { data: pageData().books}" >
    <div class="singleSearchResultLine">
        <div class="book-cover" data-bind="if: null !=  covers() && covers().length > 0"><img src="" data-bind="attr:{src: covers()[1].url}" /></div>
        <div class="book-text">
            <h2 class="book-title" ><a href="" data-bind="html: title" class="book-title-link"></a></h2>
            <%--<h4 class="page-link" data-bind="text: url"></h4>--%>
            <div class="book-description"><p class="book-content-demo" data-bind="html: description"></p></div>
            <div class="book-authors" data-bind="foreach: {data : authors}"><h4 data-bind="text: name"></h4></div>
            <div class="book-links" data-bind="foreach: {data : links}"><h4><a class="book-link" data-bind="attr : {href:url}, text : url"></a></h4></div>

        </div>
    </div>
</div>
</div>