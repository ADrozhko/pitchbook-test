<jsp:include page="include/header.jsp" />
<script src="/resources/js/index.js"></script>
<body>
    <div class="content">
        <div class="index-wrapper">
            <form action="/index" method="POST" id="indexingForm">
                <div class="index">
                    <span class="index-title">Index</span>
                    <span id="error" class="error">${error}</span>

                    <input type="text" id="indexUrl" name="q"  class="index-url-input" required>
                    <input type="submit" id="submitIndex" value="Index" class="index-submit-button" name="">
                        <%--<span id="loading" class="loading" hidden><img class="loading-image" src="/resources/img/loading.gif" />Data is indexing.</span>--%>
                    <div class="index-filter">
                        <label for="depth">Depth:</label>
                        <input type="number" name="depth" id="depth">
                    </div>
                </div>

            </form>
        </div>
    </div>
</body>
<jsp:include page="include/footer.jsp" />
