//import com.pitchbook.spd.util.UrlUtil;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//import static junit.framework.TestCase.assertNull;
//import static org.junit.Assert.assertArrayEquals;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//
//public class UrlUtilTest {
//
//    @Test
//    public void validateSubUrlTest() {
//        String result = UrlUtil.validateSubUrl("http://localhost", "/index");
//        assertEquals("http://localhost/index", result);
//        result = UrlUtil.validateSubUrl("", "/index");
//        assertEquals(null, result);
//        result = UrlUtil.validateSubUrl("http://localhost", "http://localhost");
//        assertEquals(null, result);
//
//    }
//
//    @Test
//    public void isValidUrlTest() {
//        Boolean expected[] = {true, true, true, true, false, true, false, false, false};
//        ArrayList<Boolean> actual = new ArrayList<>();
//        actual.add(UrlUtil.isValidURL("http://google.com"));
//        actual.add(UrlUtil.isValidURL("http://google.com."));
//        actual.add(UrlUtil.isValidURL("http://google.com/"));
//        actual.add(UrlUtil.isValidURL("http://google"));
//        actual.add(UrlUtil.isValidURL("google.com.ua"));
//        actual.add(UrlUtil.isValidURL("http://pitchbook.com/Research_Private_Equity_Deals.html"));
//        actual.add(UrlUtil.isValidURL(null));
//        actual.add(UrlUtil.isValidURL(""));
//        actual.add(UrlUtil.isValidURL("mailto:name@email.com"));
//        assertArrayEquals(expected, actual.toArray());
//    }
//
//    @Test
//    public void getPageTitleTest() {
//        String expected[] = {"Test title", "Test title", "Test title", "No Title", "No Title", "No Title"};
//        ArrayList<String> actual = new ArrayList<>();
//        actual.add(UrlUtil.getPageTitle("<head><title>Test title</title></head>"));
//        actual.add(UrlUtil.getPageTitle("<head><title style='border:solid black'>Test title</title></head>"));
//        actual.add(UrlUtil.getPageTitle("<head><TITLE>Test title</TITLE></head>"));
//        actual.add(UrlUtil.getPageTitle("<head>Test title</head>"));
//        actual.add(UrlUtil.getPageTitle(null));
//        actual.add(UrlUtil.getPageTitle(""));
//        assertArrayEquals(expected, actual.toArray());
//    }
//
//    @Test
//    public void getTextFromUrlTest() {
//        String result = UrlUtil.getTextFromUrl("http://pitchbook.com");
//        assertNotNull(result);
//        result = UrlUtil.getTextFromUrl("http://pitchbook");
//        assertNull(result);
//    }
//
//    @Test
//    public void removeHtmlTagsTest(){
//        String expected[] = {"Test", "Test, test"};
//        ArrayList<String> actual = new ArrayList<>();
//        actual.add(UrlUtil.removeHtmlTags("<a href='aaa'>href</a><div>Test</div><script src='/resources/scr.js'>$(document).ready(function(){})</script>"));
//        actual.add(UrlUtil.removeHtmlTags("<a href='aaa'>href</a><div>Test&rsquo test</div><script src='/resources/scr.js'>$(document).ready(function(){})</script>"));
//        assertArrayEquals(expected, actual.toArray());
//    }
//
//    @Test
//    public void getLinksFromPageTest() {
//        assertEquals(new ArrayList<String>(),UrlUtil.getLinksFromPage("<head>Test title</head>"));
//        assertNotNull(UrlUtil.getLinksFromPage("<head>Test title</head><a href='http://pitchbook.com'>Test link</a>"));
//        List<String> links =  Arrays.asList("'http://pitchbook.com'", "'/index'", "'/index/search'", "'google.com.ua'");
//        assertEquals(links, UrlUtil.getLinksFromPage("<head>Test title</head><a href='http://pitchbook.com'>Test link</a><p>Some text</p>" +
//                                                     "<ul><li><a href='/index'>Index</a></li></ul><a href='/index/search'>Search</a><div>Some block</div><a href='google.com.ua'>Google</a>"));
//    }
//
//}
